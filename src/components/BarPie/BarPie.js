import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import * as xlsx from "xlsx";
import { PieChart, Pie, Legend, Tooltip, Cell } from "recharts";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid } from "recharts";
import { LineChart, Line } from "recharts";


import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import Container from "react-bootstrap/Container";
import "./BarPie.css"

const BarPie = () => {
  const colors = [
    "#0088FE",
    "#00C49F",
    "#FFBB28",
    "#FF8042",
    "#AF19FF",
    "#FF42F9",
    "#00FFFF",
    "#FF0000",
    "#00FF00",
    "#0000FF",
    "#FF1493",
  ];

  const [data, setData] = useState([]);

    const [excelData, setExcelData] = useState([]);
    const readExcel = async (e) => {
      const file = e.target.files[0];
      const data = await file.arrayBuffer(file);
      const excelfile = xlsx.read(data);
      const excelsheet = excelfile.Sheets[excelfile.SheetNames[0]];
      const exceljson = xlsx.utils.sheet_to_json(excelsheet);
      console.log(exceljson);
  
      //passing json in setdata
      setExcelData(exceljson);
    };
  const handleFileUpload = (e) => {
    const reader = new FileReader();
    reader.readAsBinaryString(e.target.files[0]);
    reader.onload = (e) => {
      const data = e.target.result;
      const workbook = xlsx.read(data, { type: "binary" });
      const sheetName = workbook.SheetNames[0];
      const sheet = workbook.Sheets[sheetName];
      const parsedData = xlsx.utils.sheet_to_json(sheet);
      setData(parsedData);
      console.log(parsedData);
    };
  };

  const calculateCourseCount = (data) => {
    const courseCount = {};

    data.forEach((item) => {
      const course = item.Course;
      if (courseCount[course]) {
        courseCount[course] += 1;
      } else {
        courseCount[course] = 1;
      }
    });
    console.log(courseCount);
    const formattedData = Object.keys(courseCount).map((course) => ({
      name: course,
      count: courseCount[course],
    }));

    return formattedData;
  };

  const formattedData = calculateCourseCount(data);
  console.log(formattedData);
  return (
    <div>
      <Navbar style={{ backgroundColor: "rgb(24,163,174)" }}>
        <Container>
          <Navbar.Brand href="#home">Home</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="#link">Link</Nav.Link>
              <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">BarGraph</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">PieChart</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">
                  LineGraph
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
{/* 
      <input
        className="input"
        type="file"
        accept=".xlsx, .xls"
        onChange={handleFileUpload}
      /> */}
      <div
        className="Input_button"
        style={
          {
            // marginLeft: "35%",
          }
        }
      >
        <Form.Group
          controlId="formFileSm"
          className="mb-3"
          style={{
            border: "1px solid black",
            width: "max-content",
            background: "white",
          }}
        >
          <Form.Control
            onChange={(e) => readExcel(e)}
            type="file"
            size="sm"
            accept=".xlsx, .xlx"
          />
        </Form.Group>

        <div className="mt-4">
          <table className="table">
            <thead>
              <tr>
                <th>S.NO.</th>
                <th>Name</th>
                <th>Course</th>
                <th>Joined</th>
              </tr>
            </thead>
            <tbody>
              {excelData.map((getdata, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{getdata.Name}</td>
                  <td>{getdata.Course}</td>
                  <td>{getdata.JOINED}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>

      {/* <PieChart width={400} height={400}>
        <Pie
          data={formattedData}
          dataKey="count"
          nameKey="name"
          cx="50%"
          cy="50%"
          outerRadius={80}
          fill="#8884d8"
          label
        >
          {formattedData.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={colors[index % colors.length]} />
          ))}
        </Pie>
        <Tooltip />
        <Legend />
      </PieChart> */}

      {/* <div>
        <div style={{ height: "400px", width: "600px", margin: "20px auto" }}>
          <BarChart width={600} height={400} data={formattedData}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="count" fill="#0088FE" />
          </BarChart>
        </div>
      </div> */}

      {/* <div>
        <LineChart width={600} height={400} data={formattedData}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Line
            type="monotone"
            dataKey="count"
            name="Course"
            stroke="#8884d8"
          />
        </LineChart>
      </div> */}
    </div>
  );
};

export default BarPie;


