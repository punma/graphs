import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import * as xlsx from "xlsx";

function Rest() {
  //setting state
  const [excelData, setExcelData] = useState([]);

  //maping json data

  // const getChartData = () => {
  //   return excelData.map((getdata, index) =>{

  //   })
  // }

  //excel into json
  const readExcel = async (e) => {
    const file = e.target.files[0];
    const data = await file.arrayBuffer(file);
    const excelfile = xlsx.read(data);
    const excelsheet = excelfile.Sheets[excelfile.SheetNames[0]];
    const exceljson = xlsx.utils.sheet_to_json(excelsheet);
    console.log(exceljson);

    //passing json in setdata
    setExcelData(exceljson);
  };

  return (
    <>
      <h2>Himanshu</h2>
      <div
        className="Input_button"
        style={
          {
            // marginLeft: "35%",
          }
        }
      >
        <Form.Group
          controlId="formFileSm"
          className="mb-3"
          style={{
            border: "1px solid black",
            width: "max-content",
            background: "white",
          }}
        >
          <Form.Control
            onChange={(e) => readExcel(e)}
            type="file"
            size="sm"
            accept=".xlsx, .xlx"
          />
        </Form.Group>

        <div className="mt-4">
          <table className="table">
            <thead>
              <tr>
                <th>S.NO.</th>
                <th>Name</th>
                <th>Course</th>
                <th>Joined</th>
              </tr>
            </thead>
            <tbody>
              {excelData.map((getdata, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{getdata.Name}</td>
                  <td>{getdata.Course}</td>
                  <td>{getdata.JOINED}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}

export default Rest;
